#! /usr/bin/env python

import datetime
import subprocess

devicelist = [ dict(name='Server', ip='10.10.0.1')
        , dict(name='Devenv', ip='10.10.0.10')
        , dict(name='VME Source', ip='10.10.0.15')
        , dict(name='IPC Source', ip='10.10.0.16')
        ]

for device in devicelist:
    ping = subprocess.Popen(['/usr/bin/ping', '-c', '1', device['ip']], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    with open('results.txt', 'a') as fh:
        ping.wait() # Wait for process to complete

        dt = datetime.datetime.now()
        code = ping.returncode

        line = dt.isoformat('T') + ' ' + device['name'] + '(' + device['ip'] + ') => '
        if code == 0:
            line = line + 'online'
        elif code == 1:
            line = line + 'offline'
        else:
            line = line + 'connection problem'

        # Print result to stdout and file
        print(line)
        fh.write(line)
        fh.write('\n')
    ping.stdout.close()
